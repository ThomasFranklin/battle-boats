
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Thomas
 */
public class Player {
    //Player class has five fields:
    public String name;                      //Player name (used in multiplayer mode)
    public int[][] boardFirstPerson;         //Players board viewed as that player
    public int[][] boardThirdPerson;         //Players board viewed as other player (used in single player mode)
    public int[] shot;                       //Coordinates of the players shot for that turn
    public int numberOfHits;                 //Self explanatory. On a separate note, this value determines when to end the game
    
    
    //Player has two constructors:
    
    //Main Constructor
    public Player(String name, int[][] boardFirstPerson, int[][] boardThirdPerson, int[] shot, int numberOfHits){
        this.name = name;
        this.boardFirstPerson = boardFirstPerson;
        this.boardThirdPerson = boardThirdPerson;
        this.shot = shot;
        this.numberOfHits = numberOfHits;
    }
    //Default Constructor
    public Player(){
        name = "Player";
        boardFirstPerson = new int[10][10];
        boardThirdPerson = new int[10][10];
        shot = new int[2];
        numberOfHits = 0;
    }
    
    
    //Methods:
    
    //Initializes how the board looks to the opposing player
    public void initializeFirstPersonBoard(){
        for(int row=0 ; row<10 ; row++)
            for(int col=0 ; col<10 ; col++)
                boardFirstPerson[row][col] = 0;
    }
    
    //Initializes how the board looks to the opposing player
    public void initializeThirdPersonBoard(){
        for(int row=0 ; row<10 ; row++)
            for(int col=0 ; col<10 ; col++)
                boardThirdPerson[row][col] = 0;
    }
    
    //Draws the board to the console
    public void showBoardFirstPerson(){
        System.out.println("\t1 \t2 \t3 \t4 \t5 \t6 \t7 \t8 \t9 \t10");
        System.out.println();
        
        for(int row=0 ; row < 10 ; row++ ){
            System.out.print((row+1)+"");
            for(int column=0 ; column < 10 ; column++ ){
                if(boardFirstPerson[row][column] == 0){
                    System.out.print("\t"+"~");
                }
                else if(boardFirstPerson[row][column]==-1){
                    System.out.print("\t"+"O");
                }
                else if(boardFirstPerson[row][column]==1){
                    System.out.print("\t"+"X");
                }
            }
            System.out.println();
        }
    }
    
    //Draws the board to the console
    public void showBoardThirdPerson(){
        System.out.println("\t1 \t2 \t3 \t4 \t5 \t6 \t7 \t8 \t9 \t10");
        System.out.println();
        
        for(int row=0 ; row < 10 ; row++ ){
            System.out.print((row+1)+"");
            for(int column=0 ; column < 10 ; column++ ){
                if(boardThirdPerson[row][column] == 0){
                    System.out.print("\t"+"~");
                }
                else if(boardThirdPerson[row][column]==-1){
                    System.out.print("\t"+"O");
                }
                else if(boardThirdPerson[row][column]==1){
                    System.out.print("\t"+"X");
                }
            }
            System.out.println();
        }
    }
    
    //Takes in inputs from the user and stores them as a shot (int[])
    public void askForInputs(){
        Scanner input = new Scanner(System.in);
        
        System.out.println("Where will you aim the torpedo?");
        
        System.out.println("Row:");
        shot[0] = input.nextInt();
        shot[0]--;
        
        System.out.println("Column:");
        shot[1] = input.nextInt();
        shot[1]--;
    }
    
    //Checks if the given shot was a hit
    //NOTE: ONLY FOR SINGLE PLAYER MODE
    public boolean checkIfHitSinglePlayer(){
        return (boardFirstPerson[shot[0]][shot[1]] == 1);
    }
    
    //Updates the board to reflect the players inputs
    public void changeBoard(){
        if(checkIfHitSinglePlayer()){
            boardThirdPerson[shot[0]][shot[1]] = 1;
        }
        else
            boardThirdPerson[shot[0]][shot[1]] = -1;
    }
    //Asks for the player's name and then changes the name field
    public void askForName(int n){
        Scanner input = new Scanner(System.in);
        
        System.out.println("Enter name for player " + n + ":");
        name = input.nextLine();
        System.out.println("Player's name has been set to " + name);
        System.out.println();
    }
    
    //Adds on to the hit counter
    public void addHit(){
        numberOfHits++;
    }
}
