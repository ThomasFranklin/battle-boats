
import java.util.Scanner;


public class RunGame {
    public static void main(String[] args){
        
        System.out.println("Welcome to BattleBoats!");
        System.out.println("  Choose which mode to play");
        System.out.println("   For Single Player, enter \"1\"");
        System.out.println("   For Multi Player mode, enter \"2\"");
        //System.out.println("   For *under construction* enter \"3\"");
        runSelectedMode();
    }
    
    //Takes in input to select game mode and then runs that mode
    public static void runSelectedMode(){
        Scanner input = new Scanner(System.in);
        System.out.println("Enter Game Mode:");
        int selection = input.nextInt();
        if(selection == 1){
            SinglePlayer.runSinglePlayer();
        }
        else if(selection == 2){
            MultiPlayer.runMultiPlayer();
        }
        else{
            System.out.println("Please enter a valid response");
            runSelectedMode();
        }
    }
}
