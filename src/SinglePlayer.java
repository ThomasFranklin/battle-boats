
import java.util.ArrayList;


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Thomas
 */
public class SinglePlayer {
    private static int numberOfTurns = 0;
    
    //Methods used to run single player mode:
    
    public static void runSinglePlayer(){
        
        //Creates a player
        Player player = new Player();
        
        //Creating the initial 5 ships (no location or direction)
        Ship patrolBoat = new Ship("Patrol Boat", 0, 0, 1, 0);
        Ship submarine = new Ship("Submarine", 0, 0, 2, 0);
        Ship cruiser = new Ship("Cruiser", 0, 0, 2, 0);
        Ship battleship = new Ship("Battleship", 0, 0, 3, 0);
        Ship aircraftCarrier = new Ship("Aircraft Carrier", 0, 0, 4, 0);
        
        //Places each ship on a list (from longest to shortest)
        ArrayList<Ship> listOfShips = new ArrayList(5);
        listOfShips.add(aircraftCarrier);
        listOfShips.add(battleship);
        listOfShips.add(cruiser);
        listOfShips.add(submarine);
        listOfShips.add(patrolBoat);
        
        player.initializeFirstPersonBoard();
        player.initializeThirdPersonBoard();
        CommonMethods.autoHideShips(player, listOfShips);
        
        
        do{
            player.showBoardThirdPerson();
            player.askForInputs();
            numberOfTurns++;
            
            if(player.checkIfHitSinglePlayer()){
                System.out.println("\nIt's a hit! \nReloading torpedoes...\n");
                player.addHit();
            }
            else
                System.out.println("\nOops, looks like that's a miss. \nReloading torpedoes...\n");
            
            player.changeBoard();
        }
        while (player.numberOfHits < 17);
        
        System.out.println("\n\nCongradulations! You destroyed all the enemy ships!");
        System.out.println("Torpedoes Used:" + numberOfTurns);
        player.showBoardThirdPerson();
    }
}