
import java.util.ArrayList;
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Thomas
 */
public class MultiPlayer {
    
    private static int numberOfTurns = 1;
    
    //Methods used to run MultiPlayer mode:
    
    public static void runMultiPlayer(){
        
        System.out.println();
        
        //Creates two new players
        Player player1 = new Player();
        Player player2 = new Player();
        
        //Creating the initial 5 ships (no location or direction)for player1
        Ship patrolBoat1 = new Ship("Patrol Boat", 0, 0, 1, 0);
        Ship submarine1 = new Ship("Submarine", 0, 0, 2, 0);
        Ship cruiser1 = new Ship("Cruiser", 0, 0, 2, 0);
        Ship battleship1 = new Ship("Battleship", 0, 0, 3, 0);
        Ship aircraftCarrier1 = new Ship("Aircraft Carrier", 0, 0, 4, 0);
        
        //Places each ship on a list (from longest to shortest)for player1
        ArrayList<Ship> listOfShipsPlayer1 = new ArrayList(5);
        listOfShipsPlayer1.add(aircraftCarrier1);
        listOfShipsPlayer1.add(battleship1);
        listOfShipsPlayer1.add(cruiser1);
        listOfShipsPlayer1.add(submarine1);
        listOfShipsPlayer1.add(patrolBoat1);
        
        //Creating the initial 5 ships (no location or direction)for player2
        Ship patrolBoat2 = new Ship("Patrol Boat", 0, 0, 1, 0);
        Ship submarine2 = new Ship("Submarine", 0, 0, 2, 0);
        Ship cruiser2 = new Ship("Cruiser", 0, 0, 2, 0);
        Ship battleship2 = new Ship("Battleship", 0, 0, 3, 0);
        Ship aircraftCarrier2 = new Ship("Aircraft Carrier", 0, 0, 4, 0);
        
        //Places each ship on a list (from longest to shortest)for player2
        ArrayList<Ship> listOfShipsPlayer2 = new ArrayList(5);
        listOfShipsPlayer2.add(aircraftCarrier2);
        listOfShipsPlayer2.add(battleship2);
        listOfShipsPlayer2.add(cruiser2);
        listOfShipsPlayer2.add(submarine2);
        listOfShipsPlayer2.add(patrolBoat2);
        
        System.out.println("Welcome to Multi Player Mode!\n");
        
        player1.askForName(1);
        player2.askForName(2);
        
        player1.initializeFirstPersonBoard();
        player2.initializeFirstPersonBoard();
        
        player1.initializeThirdPersonBoard();
        player2.initializeThirdPersonBoard();
        
        System.out.println("\nIt's " + player1.name + "\'s turn to hide ships.\n");
        CommonMethods.manualHideShips(player1, listOfShipsPlayer1);
        System.out.println("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
        System.out.println("\nIt's " + player2.name + "\'s turn to hide ships.\n");
        CommonMethods.manualHideShips(player2, listOfShipsPlayer2);
        System.out.println("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
        
        System.out.println("It's time to play the game!");
        
        do{
            if(numberOfTurns % 2 == 0){
                System.out.println("It's " + player2.name +"'s turn.");
                
                System.out.println("\n\nOpponent's Board:");
                player1.showBoardThirdPerson();
                System.out.println("\nYour Board:");
                player2.showBoardFirstPerson();
                
                player2.askForInputs();
                
                if(CommonMethods.checkIfHit(player2, player1)){
                    System.out.println("It's a hit!");
                    player1.boardThirdPerson[player2.shot[0]][player2.shot[1]] = 1;
                    player1.showBoardThirdPerson();
                    player2.addHit();
                }
                else{
                    System.out.println("Oops, looks like a miss.");
                    player1.boardThirdPerson[player2.shot[0]][player2.shot[1]] = -1;
                    player1.showBoardThirdPerson();
                }
                numberOfTurns++;
                Scanner input = new Scanner(System.in);
                System.out.println("Please enter \"1\" to end turn.");
                input.nextInt();
                System.out.println("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
            }
            else{
                System.out.println("It's " + player1.name +"'s turn.");
                
                System.out.println("\n\nOpponent's Board:");
                player2.showBoardThirdPerson();
                System.out.println("\nYour Board:");
                player1.showBoardFirstPerson();
                
                player1.askForInputs();
                
                if(CommonMethods.checkIfHit(player1, player2)){
                    System.out.println("It's a hit!");
                    player2.boardThirdPerson[player1.shot[0]][player1.shot[1]] = 1;
                    player2.showBoardThirdPerson();
                    player1.addHit();
                }
                else{
                    System.out.println("Oops, looks like a miss.");
                    player2.boardThirdPerson[player1.shot[0]][player1.shot[1]] = -1;
                    player2.showBoardThirdPerson();
                }
                numberOfTurns++;
                Scanner input = new Scanner(System.in);
                System.out.println("Please enter \"1\" to end turn.");
                input.nextInt();
                System.out.println("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
            }
        }
        while((player1.numberOfHits < 17)||(player2.numberOfHits < 17));
        
        if(player1.numberOfHits > player2.numberOfHits){
            System.out.println(player1.name + " won the game!");
        }
        else{
            System.out.println(player2.name + " won the game!");
        }
    }
}
