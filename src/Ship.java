
import java.util.Random;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Thomas
 */

public class Ship {
    //Ship has four fields:
    public String name;        //The name of the ship, conviniently stored as a string
    public int row;         //Row where the ship starts
    public int column;      //Column where the ship starts
    public int length;      //Length of the ship NOT COUNTING the initial point
    public int direction;   //Either 0 or 1. 0 being horizontal and 1 being vertical
    
    //Ship has one constructor:
    public Ship(String name, int row, int column, int length, int direction){
        this.name = name;
        this.column = column;
        this.row = row;
        this.length = length;
        this.direction = direction;
    }
        
    //Ship has two methods:
    public void chooseDirection(){
        Random random = new Random();
        direction = random.nextInt(2);
    }
    public void chooseLocation(){
        Random random = new Random();
        if(direction == 0){
            row = random.nextInt(10);
            column = random.nextInt(10 - length);
        }
        else if(direction == 1){
            row = random.nextInt(10 - length);
            column = random.nextInt(10);
        }
    }
}
