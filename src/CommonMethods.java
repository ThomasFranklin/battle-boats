
import java.util.ArrayList;
import java.util.Scanner;


public class CommonMethods {
    
    //Allows players to hide ships manually
    public static void manualHideShips(Player player, ArrayList<Ship> listOfShips){
        Scanner input = new Scanner(System.in);
        
        System.out.println(player.name + ", you need to hide your ships, one at a time on the board.");
        System.out.println("In order to hide a ship, first choose the direction of the ship, either hroizontal or vertical.");
        System.out.println("Next, enter the row number and column number you want the FRONT of the ship to start at.");
        System.out.println("PLEASE NOTE: Vertical ships face upwards, horizontal ships face to the left.");
        System.out.println("Don't let your ships go off the board or intersect other ships!");
        System.out.println("Alternatively, if this is too much effort for you, you can choose to have the computer place your ships randomly for you.\n");
        
        System.out.println("Would you like the computer to place your ships for you?");
        System.out.println("For yes, enter \"1\"\nFor no, enter \"2\"");
        System.out.println("Enter response here:");
        int response = input.nextInt();
        
        if(response == 1){
            autoHideShips(player, listOfShips);
            player.showBoardFirstPerson();
        }
        else if(response == 2){
            helperForManualHideShips(player, listOfShips, 0);
        }
        System.out.println("Congradulations! Your ships have all been placed and you are ready to play!");
        System.out.println("Enter \"1\" to continue:");
        int exit = input.nextInt();
        if(exit == 1){
            return;
        }
    }
    
    //Helper function for manualHideShips
    public static void helperForManualHideShips(Player player, ArrayList<Ship> listOfShips, int currentIndex){
        int n = currentIndex;
        for(int i=n ; i<5 ; i++){
            Ship ship = listOfShips.get(i);
            System.out.println("\n\nChoose a location for the " + ship.name +". The " + ship.name + " is " + (ship.length + 1) + " spaces long.");
            Scanner input = new Scanner(System.in);
            System.out.println("Which direction would you like the " + ship.name + " to be placed in? Enter \"0\" for horizontal and \"1\" for vertical.");
            System.out.println("Enter response here:");
            int direction = input.nextInt();
            
            if(direction == 0){
                ship.direction = 0;
                System.out.println("The direction has been set to horizontal.");
            }
            else if(direction == 1){
                ship.direction = 1;
                System.out.println("The direction has been set to vertical.");
            }
            else{
                System.out.println("\n Please enter a valid response. Try again.");
                helperForManualHideShips(player, listOfShips, n);
            }
            System.out.println("\nNow enter the row and column number, 1-10, where you would like the ship to start from.");
            System.out.println("REMEMBER: DO NOT place your ship where any part of it goes off the edge or intersects other ships.");
            if(direction == 0){
                System.out.println("You chose to make the " + ship.name + " horizontal. This means that the point you choose will be the left edge of the ship.");
            }
            else if(direction == 1){
                System.out.println("You chose to make the " + ship.name + " vertical. This means that the point you choose will be the top edge of the ship.");
            }
            System.out.println("Where will you place the boat?");
            System.out.println("Row:");
            int chosenRow = input.nextInt();
            chosenRow--;
            ship.row = chosenRow;
            
            System.out.println("Column:");
            int chosenColumn = input.nextInt();
            chosenColumn--;
            ship.column = chosenColumn;
            
            System.out.println("Checking to see if coordinates are valid...");
            if(manualShipLooper(player, ship, n, listOfShips)){
                System.out.println("Invalid location entered. Please try again.");
                System.out.println("REMEMBER: Do not place the ship off the edge of the board or over other ships.");
                helperForManualHideShips(player, listOfShips, n);
                break;
            }
            else{
                placeShipOnBoard(player, ship);
                System.out.println("\nSuccess! The " + ship.name + " has been placed.");
                n++;
                player.showBoardFirstPerson();
            }
        }
    }
    
    //Hides ships in the board:
    public static void autoHideShips(Player player, ArrayList<Ship> listOfShips){
        for(int i=0 ; i<5 ; i++){
            Ship ship = listOfShips.get(i);
            ship.chooseDirection();
        }
        for(int i=0 ; i<5 ; i++){
            Ship ship = listOfShips.get(i);
            ship.chooseLocation();
            
            //Checks to see if that location is legal, based on:
            //Size and direction relative to other ships
            shipLooper(player, ship);
            placeShipOnBoard(player, ship);
        }
    }
    
    //Helper function to autoHideShips
    //Repetitively chooses the location of a new ship until the new ship doesn't intersect previous ones
    public static void shipLooper(Player player, Ship currentShip){
        do{
            currentShip.chooseLocation();
        }
        while(checkIfShipsIntersect(player, currentShip));
    }
    
    //Helper Function
    public static boolean manualShipLooper(Player player, Ship currentShip, int currentIndex, ArrayList<Ship> listOfShips){
        return (checkIfShipsOnBoard(currentShip)||checkIfShipsIntersect(player, currentShip));
    }
    
    //Checks to see if a ship goes off the edge of the board
    public static boolean checkIfShipsOnBoard(Ship currentShip){
        return (((currentShip.direction == 0)&&
                 (9 - currentShip.column < currentShip.length))||
                ((currentShip.direction == 1)&&
                 (9 - currentShip.row < currentShip.length))
                );
    }
 

    //Checks if the given inputs are a hit or a miss
    public static boolean checkIfHit(Player attackingPlayer, Player defendingPlayer){
        return (defendingPlayer.boardFirstPerson[attackingPlayer.shot[0]][attackingPlayer.shot[1]] == 1);
    }
    
    //Places ships on the player's board.
    public static void placeShipOnBoard(Player player, Ship ship){
        if(ship.direction == 0){
            for(int i=ship.column ; i<(ship.column + ship.length + 1) ; i++){
                player.boardFirstPerson[ship.row][i] = 1;
            }
        }
        if(ship.direction == 1){
            for(int i=ship.row ; i<(ship.row + ship.length +1) ; i++){
                player.boardFirstPerson[i][ship.column] = 1;
            }
        }
    }
    
    public static boolean checkIfShipsIntersect(Player player, Ship ship){
        if(ship.direction == 0){
            for(int i=ship.column ; i<(ship.column + ship.length + 1) ; i++){
                if(player.boardFirstPerson[ship.row][i] == 1){
                    return true;
                }
            }
        }
        if(ship.direction == 1){
            for(int i=ship.row ; i<(ship.row + ship.length +1) ; i++){
                if(player.boardFirstPerson[i][ship.column] == 1)
                    return true;
            }
        }
        return false;
    }
}
